<?php

namespace App\Controller;

use App\Entity\Entreprise;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/entreprises')]
class EntrepriseController extends AbstractController
{
    #[Route('/', name: 'app_entreprises')]
    public function index(ManagerRegistry $doctrine): Response
    {
        // Renvoyer tous les salariés
        $repository = $doctrine->getRepository(Entreprise::class);
        $entreprises = $repository->findAll();

        return $this->render('entreprise/index.html.twig', [
            'controller_name' => 'EntrepriseController',
            'entreprises' => $entreprises
            // 'entreprises' => ['crash', 'coco']
        ]);
    }

    #[Route('/{id}', name: 'entreprise_show')]
    public function show(Entreprise $entreprise): Response
    {
        return $this->render('entreprise/show.html.twig', [
            'entreprise' => $entreprise
        ]);
    }
}