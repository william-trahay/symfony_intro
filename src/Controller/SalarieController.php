<?php

namespace App\Controller;

use App\Entity\Salarie;
use App\Entity\Entreprise;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SalarieController extends AbstractController
{
    #[Route('/salarie/add', name: 'salarie_add')]
    #[Route('/salarie/{id}/edit', name: 'salarie_edit')]
    public function addEditSalarie(Salarie $salarie = null, Request $request, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        if(!$salarie) {
            $salarie = new Salarie();
        }

        $form = $this->createFormBuilder($salarie)
            ->add('nom', TextType::class)
            ->add('prenom', TextType::class)
            ->add('dateNaissance', DateType::class, [
                'years' => range(date('Y'), date('Y')-70),
                'label' => 'Date de naissance',
                'format' => 'ddMMMMyyyy'
            ])
            ->add('adresse', TextType::class)
            ->add('cp', TextType::class)
            ->add('ville', TextType::class)
            ->add('dateEmbauche', DateType::class, [
                'years' => range(date('Y'), date('Y')-70),
                'label' => 'Date d\'embauche',
                'format' => 'ddMMMMyyyy'
            ])
            ->add('Entreprise', EntityType::class, [
                'class' => Entreprise::class,
                'choice_label' => 'raisonSociale',
            ])
            ->add('Valider', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($salarie);
            $entityManager->flush();

            return $this->redirectToRoute('app_salaries');
        }

        return $this->render('salarie/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    #[Route('/salaries', name: 'app_salaries')]
    public function index(ManagerRegistry $doctrine): Response
    {
        // Renvoyer tous les salariés
        $repository = $doctrine->getRepository(Salarie::class);
        // $salaries = $repository->findAll();
        $salaries = $repository->getAllOrderByDateDembauche();

        return $this->render('salarie/index.html.twig', [
            'controller_name' => 'SalarieController',
            'salaries' => $salaries
            // 'salaries' => ['crash', 'coco']
        ]);
    }

    #[Route('/salarie/{id}', name: 'salarie_show')]
    public function show(ManagerRegistry $doctrine, int $id): Response
    {
        $repository = $doctrine->getRepository(Salarie::class);
        $salarie = $repository->find($id);

        // dd($salarie);

        return $this->render('salarie/show.html.twig', [
            'salarie' => $salarie
        ]);
    }
}


// #[Route('/salarie/{id}', name: 'salarie_show')]
// public function show(Salarie $salarie): Response
// {
//     return $this->render('salarie/show.html.twig', [
//         'salarie' => $salarie
//     ]);
// }
