## Symfony Intro

https://symfony.com/doc/current/setup.html

- installer symfony (https://symfony.com/download)
- installer composer (https://getcomposer.org/download/)
- créer projet `symfony new symfony_intro  --version="5.4.*" --webapp`
- créer une base de données MYSQL _symfony_intro_
- paramétrer l'accès à la base de données dans le projet: sur le fichier .env, commenter la ligne POSTGRESQL, décommenter la ligne MYSQL et remplacer l'utilisateur, le mot de passe et le nom de bdd (DATABASE_URL="mysql://root:password@127.0.0.1:3306/nomdebdd?serverVersion=8&charset=utf8mb4")
- lancer le projet : `symfony server:start` (pour arréter les serveur, `symfony server:stop`)

Définir la logique metier:

- définir les entités : `php bin/console make:entity`
- définir les controleurs : `php bin/console make:controller`
  https://symfony.com/doc/5.4/doctrine.html
- implémenter les répositories : Ils ont été automatiquement créés

Migration de la base de données:

- créer le fichier de migration `php bin/console make:migration`
- appliquer la migration `php bin/console doctrine:migrations:migrate` puis `yes`

Pour mettre à jour le schéma de la base:

- php bin/console doctrine:schema:update --complete
